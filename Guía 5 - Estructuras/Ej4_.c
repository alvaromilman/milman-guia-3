/*Una ferretería tiene un listado de facturas emitidas en cierto año,
con estos datos: número de factura, fecha de emision(dia y mes),
nombre del cliente y monto total. Dado un vector de 10 facturas,
se necesitan mostrar en pantalla cual fue el mejor mes
(o sea el de mayor dinero facturado). Los datos se pueden ingresar por
teclado o dejarlos fijos en el programa para no perder tiempo en tipear datos.
*/

#include<stdio.h>

int i, mayor=0;
float MESES[12]={0,0,0,0,0,0,0,0,0,0,0,0};

typedef struct FECH
{
    int DIA;
    int MES;
}fecha;
struct FACTURAS
{
    int NUMERO;
    fecha FECHA;
    char CLIENTE[25];
    float MONTO;
} FAC[10];


main()
{
    for(i=0;i<=1;i++)
        FAC[i].NUMERO = i+1;

    for(i=0;i<=1;i++){
        printf("Ingrese fecha:\nDia:(N%cmero)\t", 163);
        scanf("%d", &FAC[i].FECHA.DIA);
        printf("Mes:(N%cmero)\t",163);
        scanf("%d", &FAC[i].FECHA.MES);
        printf("Ingrese cliente:\n");
        scanf("%s", &FAC[i].CLIENTE);
        printf("Ingrese monto:\n");
        scanf("%f", &FAC[i].MONTO);
    }

    for(i=0;i<=1;i++){
        MESES[FAC[i].FECHA.MES-1]=MESES[FAC[i].FECHA.MES-1]+FAC[i].MONTO;
    }

    for(i=1;i<=11;i++){
        if(MESES[i]>=MESES[mayor])
            mayor=i+1;
    }
    printf("El mejor mes fue: %d", mayor);

}
