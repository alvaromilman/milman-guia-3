/*Utilice el Ej 1, modificandolo paraque las notas des estudiante
 están en un vector de notas dentro de la estructura. Este vector
 de notas puede almacenar hasta 10 notas del alumno. Los lugares
 no utilizados se escriben con un valor -1 para indicar q ese lugar
 está vacio. De esta manera, un estudiante puede haber rendido dos
 exámenes, otro 4 y otro 10, para citar algunos ejs. Se pide conservar
 la funcionalidad del programa teniendo en cuenta esta nueva organizacion
 de la info. Por lo tanto, para mostrar el promedio del estudiante, habrá
 que considerar las notas que tiene en ese vector de notas, teniendo cuidado
 en utilizar solamente las notas que existan y omitiendo los -1 que pueda haber.

*/

#include<stdio.h>
int i=0, j=0, CONT=0, cant=0;
float PROM;
 struct ALUMNOS
{
    int DNI;
    float NOTAS[10];
} alum[5];



main()
{
for(i=0;i<=4;i++){
    for(j=0;j<=9;j++){
        alum[i].NOTAS[j]=-1;
    }
}

for (i=0;i<=4;i++){
printf("Ingrese los datos del alumno %d:", i+1);
printf("\nDNI:");
scanf("%d", &alum[i].DNI);
printf("Cantidad de notas:");
scanf("%d", &cant);
    for(j=0; j<cant;j++){
        printf("\nIngrese la nota %d: ", j+1);
        scanf("%f", &alum[i].NOTAS[j]);
    }
}



for (i=0;i<=4;i++){
printf("\nAlumno %d: ", i+1);
printf("\nDNI: %d", alum[i].DNI);
PROM=0; CONT=0; j=0;
    while(alum[i].NOTAS[j]!=-1){
        PROM=alum[i].NOTAS[j] + PROM;
        j++;
        CONT++;
    }
printf("\nPROMEDIO: %.2f", PROM / CONT);
}


}

