/*Una aerolínea tiene vuelos, los cuales poseen un código
alfanumérico (ej: AR1500), una ciudad de origen y una ciudad destino.
Ingrese 4 vuelos en un vector. Luego de ingresados los datos permita
que el usuario escriba el nombre de una ciudad y muestre los vuelos
que parten y los que arriban a esa ciudad. Si no hubiera vuelos para
la ciudad ingresada debe mostrarse un mensaje de error.
*/

#include<stdio.h>
int i=0;
char CIUDAD[25];

 struct VUELOS
{
    char CODE[7];
    char ORIG[25];
    char DEST[25];
} ALAS[4];

main()

{

for (i=0;i<=3;i++){
    printf("Ingrese datos del vuelo %d:", i+1);
    printf("\nCodigo:");
    scanf("%s", &ALAS[i].CODE);
    printf("\nOrigen:");
    scanf("%s", &ALAS[i].ORIG);
    printf("\nDestino:");
    scanf("%s", &ALAS[i].DEST);
}

printf("\n\nIngrese la ciudad a buscar:");
scanf("%s", &CIUDAD);

printf("\n\nVuelos con origen %s:", CIUDAD);
for (i=0;i<=3;i++){
    if(!strcmp(ALAS[i].ORIG, CIUDAD)){
        printf("\n%s", ALAS[i].CODE);
    }
}

printf("\n\nVuelos con destino %s:", CIUDAD);
for (i=0;i<=3;i++){
    if(!strcmp(ALAS[i].DEST, CIUDAD)){
        printf("\n%s", ALAS[i].CODE);
    }
}
}



