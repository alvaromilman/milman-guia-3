/*Declare una estructura para almacenar datos de estudiantes
(DNI y dos notas correspondientes a los dos cuatrimestres del año).
Hagaun programa que permita ingresar los datos de 5 estudiantes en
unvector de estas estructuras. Luego de ingresar los datos se deben
mostrarlos DNI de cada estudiante y el promedio que tiene en sus examenes.
*/

#include<stdio.h>
int i=0;
 struct ALUMNOS
{
    int DNI;
    float cuat1;
    float cuat2;
} alum[5];


main()

{

for (i=0;i<=4;i++){
printf("Ingresedatos del alumno %d:", i+1);
printf("\nDNI:");
scanf("%d", &alum[i].DNI);
printf("\nNOTA del 1er cuatrimestre:");
scanf("%f", &alum[i].cuat1);
printf("\nNOTA del 2do cuatrimestre:");
scanf("%f", &alum[i].cuat2);
}

for (i=0;i<=4;i++){
printf("\nAlumno %d: ", i+1);
printf("\nDNI: %d", alum[i].DNI);
printf("\nPROMEDIO: %.2f", (alum[i].cuat1 + alum[i].cuat2)/2);
}


}
