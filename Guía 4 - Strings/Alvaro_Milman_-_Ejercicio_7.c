/*El usuario ingresará 5 nombres de personas y
sus edades (número entero). Luego de finalizar
el ingreso, muestre en pantalla el nombre de la
persona más joven. El ingreso se realiza de este
modo: nombre y edad de la primera persona, luego
nombre y edad de la segunda, etc... Nota: no hay
 que almacenar todos los nombres y todas las edades.
*/

#include<stdio.h>

char NOMBRE[20];
char NOMBRE_M[20];

int i = 1, EDAD = 0, EDAD_M;

main(){
    for(i=1;i<=5;i++){
        printf("Ingrese el nombre de la persona %d:\n", i);
        scanf("%s", &NOMBRE);                                           //Guardo el nombre

        printf("Ingrese la edad de la persona %d:\n", i);
        scanf("%d", &EDAD);                                             //Guardo la edad

        if(EDAD < EDAD_M || i == 1){                                   //Comparo la edad ingresada con la menor ingresada hasta el momento
            EDAD_M = EDAD;                                             //Si la edad es menor la Guardo y guardo el nombre de la persona
            strcpy(NOMBRE_M, NOMBRE);
        }
    }

    printf("\nEl/La mas joven es: %s", NOMBRE_M);
}
