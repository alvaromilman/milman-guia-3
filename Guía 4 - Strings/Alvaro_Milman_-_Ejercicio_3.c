/*El usuario ingresa dos strings. Mostrar en
pantalla si son iguales o no, es decir, si
tienen las mismas letras en las mismas posiciones.
*/

#include <stdio.h>

char PALABRA1[20];
char PALABRA2[20];
int IGUALES = 0;
main(){
printf("Ingrese la primera palabra (M%cximo 20 letras):\n", 160);
scanf("%s", &PALABRA1);                                                      //Guardo la primera palabra en un vector
printf("Ingrese la segunda palabra (M%cximo 20 letras):\n", 160);
scanf("%s", &PALABRA2);                                                     //Guardo la segunda palabra en otro vector

for (int i=0;PALABRA1[i]!=NULL || PALABRA2[i]!=NULL;i++){
    if(PALABRA1[i]!=PALABRA2[i]) {IGUALES = 1; break;}                  //Comparo las letras de cada palabra que se encuentran en la misma posición
}
if(IGUALES==0) printf("Las palabras son iguales");

else printf("Las palabras son diferentes");

}
