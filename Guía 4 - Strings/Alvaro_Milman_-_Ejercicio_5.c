/*El usuario ingresa una palabra. Mostrar
en pantalla cuántas vocales minúsculas y
mayúsculas contiene.
*/

#include <stdio.h>

char PALABRA[20];
int vocales = 0, VOCALES = 0;

main(){

printf("Ingrese una palabra (M%cximo 20 letras):\n", 160);
scanf("%s", &PALABRA);                                            //Guardo la palabra en un vector

for (int i=0;PALABRA[i]!=NULL;i++){
    if(PALABRA[i] == 'a' || PALABRA[i] == 'e' || PALABRA[i] == 'i' || PALABRA[i] == 'o' || PALABRA[i] == 'u') vocales++;  //Cuento las vocales mayúsculas
    if(PALABRA[i] == 'A' || PALABRA[i] == 'E' || PALABRA[i] == 'I' || PALABRA[i] == 'O' || PALABRA[i] == 'U') VOCALES++;  //Cuento las vocales minúsculas
}
printf("La palabra tiene %d vocales min%csculas y %d vocales may%csculas", vocales, 163, VOCALES, 163);
}
