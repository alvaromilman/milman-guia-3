/*El usuario ingresará una palabra de hasta 10
letras. Se desea mostrarla en pantalla pero
encriptada según el "Código César". Esto consiste
en reemplazar cada letra con la tercera consecutiva
en el abecedario. Por ejemplo, "CASA" se convierte
en "FDVD". Tener en cuenta que las últimas letras
deben volver al inicio, por ejemplo la Y se convierte B.
Este mecanismo se utilizaba en el Imperio Romano.
*/

#include<stdio.h>

char PALABRA[10];


main(){

    printf("Ingrese la palabra (MAYUSCULAS):\n");
    scanf("%s", PALABRA);                                     //Guardo la palabra ingresada

    printf("Palabra encriptada:\n");

    for(int i = 0; i < strlen(PALABRA);i++){                   //Recorro el vector
        if(PALABRA[i] < 'X' ){                                 //si la letra es anterior a la X la cambio por la tercera consecutiva
            printf("%c", PALABRA[i] + 3);                      
        }
        if(PALABRA[i] == 'X') printf("A");                     //Las letras que es necesario las reemplazo por la letra del inicio que corresponde
        if(PALABRA[i] == 'Y') printf("B");
        if(PALABRA[i] == 'Z') printf("C");
    }

}
