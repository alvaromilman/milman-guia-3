/*Permitir el ingreso de una palabra y mostrarla
en pantalla al revés. Por ejemplo, para "CASA"
se debe mostrar "ASAC".
*/

#include <stdio.h>

char PALABRA[20];
int LETRAS = 0, i;
main(){
printf("Ingrese una palabra (M%cximo 20 letras):\n", 160);
scanf("%s", &PALABRA);                                               //Guardo la palabra en el vector

for (i=0;PALABRA[i]!=NULL;i++){
    LETRAS++;                                                       //Cuento la cantidad de letras
}

for (i = LETRAS - 1;i>=0;i--){
    printf("%c", PALABRA[i]);                                       //Imprimo el vector al revés
}

}


