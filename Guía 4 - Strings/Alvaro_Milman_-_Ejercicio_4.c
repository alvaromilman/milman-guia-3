/*El usuario ingresa una palabra. Mostrar en
pantalla cuántas letras A minúsculas contiene.
*/

#include <stdio.h>

char PALABRA[20];
int LETRAS_a = 0;

main(){

printf("Ingrese una palabra (M%cximo 20 letras):\n", 160);
scanf("%s", &PALABRA);                                             //Guardo la palabra en un vector

for (int i=0;PALABRA[i]!=NULL;i++){
    if(PALABRA[i]== 'a') LETRAS_a++;                              //Recorro el vector y cuento la cantidad de a minúsculas
}
printf("La palabra tiene %d letras a", LETRAS_a);
}
