/*El usuario ingresa una palabra. Determinar qué
letra aparece mayor cantidad de veces. Para
simplificar el problema, trabaje solo con mayúsculas.
*/

#include <stdio.h>

char PALABRA[20];
int LETRAS[20], mayor=0, mayor2=0, FLG=0, TOPE=0, i;

main(){

printf("Ingrese una palabra (M%cximo 20 letras):\n", 160);
scanf("%s", &PALABRA);                                             //Guardo la palabra en un vector

for (i=0;PALABRA[i]!=NULL;i++){
    for(int x=i+1;PALABRA[x]!=NULL;x++){                          //Comparo las letras de la palabras con las siguientes
        if(PALABRA[i]==PALABRA[x])LETRAS[i]++;                    //Sumo 1 en la posición de la letra por cada vez que se repita
    }
}

for (i=0;PALABRA[i]!=NULL;i++){
    TOPE++;                                                   //Cuento cantidad de letras de la palabra
}

for(i=0;i<TOPE;i++){
    if(LETRAS[i]==0) FLG++;                                   //Reviso que se repita al menos una vez alguna letra
}

if(FLG==TOPE)
    printf("Todas las letras aparecen la misma cantidad de veces");   //Si ninguna letra se repite

else{                                                                 //Si al menos una letra se repite
    for(i=1;i<=TOPE;i++){
        if(LETRAS[i]>LETRAS[mayor])                                  //Reviso en que posición está la letra que más se repite
            mayor = i;                                               //Guardo la mayor posición
    }
    for(i=0;i<=TOPE;i++){
        if(LETRAS[i]==LETRAS[mayor] &&PALABRA[i] != PALABRA[mayor])    //Reviso si alguna letra diferente se repite la misma cantidad de veces
            mayor2 = i;                                             //Si se da el caso guardo la otra letra más repetida 
    }

if(mayor2==0)
    printf("La letra que aparece m%cs veces es: '%c'", 160, PALABRA[mayor]);  //Si solo hay una letra mas repetida

else
    printf("Las letras que aparecen m%cs veces son: '%c' y '%c'", 160, PALABRA[mayor], PALABRA[mayor2]);   //Si hay 2 letras mas repetidas
}
}
