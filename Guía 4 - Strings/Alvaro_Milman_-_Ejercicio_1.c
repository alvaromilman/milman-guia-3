/*Permitir que el usuario ingrese una palabra de hasta 20
letras. Mostrar en pantalla cuántas letras tiene la
palabra ingresada. Por ejemplo "CASA" debe indicar 4 letras.
*/

#include <stdio.h>

char PALABRA[20];
int LETRAS=0;

main(){
printf("Ingrese una palabra (M%cximo 20 letras):\n", 160);
scanf("%s", &PALABRA);                                       //Guardo la palabra en el vector

for (int i=0;PALABRA[i]!=NULL;i++){
    LETRAS++;                                               //Cuento la cantidad de letras que tiene
}

printf("La palabra tiene %d letras", LETRAS);              
}
