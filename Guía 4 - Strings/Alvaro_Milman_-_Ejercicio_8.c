/*8) El usuario ingresará nombres de personas
hasta que ingrese la palabra "FIN". No sabemos
cuántos nombres ingresará. Luego de finalizar
el ingreso, mostrar en pantalla cuál es el primer
nombre en orden alfabético de todos los ingresados.
*/

#include<stdio.h>

char NOMBRE[20];
char NOMBRE_A[20]="§§§§§§§§§§";
char FIN[3]="FIN";


main(){

    while(strcmp(NOMBRE, FIN) != 0){                                              //Ejecuto el programa mientras la palabra ingresada no sea "FIN"
            printf("Ingrese un nombre:\n");
            scanf("%s", &NOMBRE);                                                 //Guardo la palabra ingresada en un vector
            if(strcmp(NOMBRE, NOMBRE_A) < 0 && strcmp(NOMBRE, FIN) != 0){         //Si la palabra es alfabéticamente menor a la guardada la guardo
                strcpy(NOMBRE_A, NOMBRE);
            }
    }
    printf("EL primer nombre alfab%cticamente es: %s", 130, NOMBRE_A);                   //Imprimo palabra guardada

}
